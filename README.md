IMUV - DEVELOPMENT KIT
-------------------

Guia para instalação completa com todos requisitos e adicionais da [IMUV](https://imuv.me).

Guia criado/atualizado para Ubuntu 24.04 LTS, podendo ser incompátivel para outras versões.

### 1. Atualização do sistema

```shell
sudo apt update && sudo apt upgrade && sudo apt install
```

### 2. Softwares e configurações essenciais

```shell
sudo apt install git curl libfuse2
```

### 3. Softwares de comunicação

Na [IMUV](https://imuv.me) utilizamos principalmente o software Discord para comunicação.

#### Discord

```shell
wget -O ~/Downloads/discord.deb "https://discord.com/api/download?platform=linux&format=deb"

sudo apt install ~/Downloads/discord.deb

sudo rm ~/Downloads/discord.deb
```

ou

```shell
sudo snap install discord
```

#### Telegram

```shell
sudo snap install telegram-desktop
```

### 4. Navegadores

#### Google Chrome

```shell
wget -O ~/Downloads/google-chrome.deb "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"

sudo apt install ~/Downloads/google-chrome.deb

sudo rm ~/Downloads/google-chrome.deb
```

### 5. Docker

```shell
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt remove $pkg; done

sudo install -m 0755 -d /etc/apt/keyrings

sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc

sudo chmod a+r /etc/apt/keyrings/docker.asc

echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update && sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Para verificar a instalação:

```shell
sudo docker run hello-world
```

Para não necessitar da execução como administrador:

```shell
sudo groupadd docker

sudo usermod -aG docker $USER
```

### 6. IDE

A utilização de IDE é de escolha do desenvolvedor, mas recomendamos a utilização dos pacotes da JetBrains ou o VS Code.

#### JetBrains Toolbox

```shell
curl -fsSL https://raw.githubusercontent.com/nagygergo/jetbrains-toolbox-install/master/jetbrains-toolbox.sh | bash
```

#### VS Code

```shell
sudo snap install code --classic
```

### 7. Outros

#### Flameshot

```shell
sudo apt install flameshot
```

#### Figma

```shell
sudo snap install figma-linux
```

#### OBS Studio

```shell
sudo apt install obs-studio
```

#### Termius

```shell
sudo snap install termius-app
```

#### Postman

```shell
sudo snap install postman
```

#### Spotify

```shell
curl -sS https://download.spotify.com/debian/pubkey_C85668DF69375001.gpg | sudo gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/spotify.gpg

echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list

sudo apt update && sudo apt install spotify-client
```
Read [Spotify for Linux](https://www.spotify.com/br-pt/download/linux/).

#### Gerenciadores de senha

```shell
sudo snap install bitwarden
```

#### Espelhamento de telas de dispositivos móveis ([scrcpy](https://github.com/Genymobile/scrcpy))

```
snap install scrcpy
```

### 8. Adicional para GNOME (dash-to-dock, clock-show-seconds)

```shell
sudo apt install gnome-browser-connector
```

#### Sugestões gráficas

```shell
gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false

gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM

gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false

gsettings set org.gnome.shell.extensions.dash-to-dock autohide true

gsettings set org.gnome.shell.extensions.dash-to-dock intellihide true

gsettings set org.gnome.shell.extensions.dash-to-dock intellihide-mode 'FOCUS_APPLICATION_WINDOWS'

gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true

gsettings set org.gnome.shell.extensions.dash-to-dock show-mounts false

gsettings set org.gnome.shell.extensions.dash-to-dock show-trash false

gsettings set org.gnome.desktop.interface clock-show-seconds true
```

#### Sugestões de ícones fixados

```shell
gsettings set org.gnome.shell favorite-apps "[]"

gsettings set org.gnome.shell favorite-apps "['firefox_firefox.desktop', 'google-chrome.desktop', 'org.gnome.Nautilus.desktop', 'telegram-desktop_telegram-desktop.desktop', 'discord.desktop', 'org.gnome.Terminal.desktop', 'jetbrains-studio.desktop', 'jetbrains-clion.desktop', 'jetbrains-datagrip.desktop', 'jetbrains-pycharm.desktop', 'jetbrains-phpstorm.desktop', 'jetbrains-webstorm.desktop', 'code_code.desktop', 'postman_postman.desktop', 'figma-linux_figma-linux.desktop', 'com.obsproject.Studio.desktop', 'termius-app_termius-app.desktop']"
```

#### Sugestões de extensões

- [Vitals](https://extensions.gnome.org/extension/1460/vitals/)
