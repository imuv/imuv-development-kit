# CHANGELOG(Registro de alterações)

### [0.1.6] - Atualização de Spotify for Linux e scrcpy ([@thtmorais](https://gitlab.com/thtmorais))

### [0.1.5] - Ubuntu 24.04 LTS ([@thtmorais](https://gitlab.com/thtmorais))
- Sugestões de extensões
- Novos pacotes sugeridos

### [0.1.4] - Remoção de comando e chrome-gnome-shell ([@thtmorais](https://gitlab.com/thtmorais))

### [0.1.3] - Adição do fuse ([@thtmorais](https://gitlab.com/thtmorais))

### [0.1.2] - Adição de configurações do sistema e Spotify ([@thtmorais](https://gitlab.com/thtmorais))

### [0.1.1] - Ubuntu 22.04 LTS ([@thtmorais](https://gitlab.com/thtmorais))

### [0.1.0] - Extensões e ajustes em arquivos .MD ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.9] - Helm ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.8] - Ajustes no repositório ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.7] - API(Postman e Insomnia) e termius-app ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.6] - Portainer e ajustes na descrição ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.5] - OpenJDK from AdoptOpenJDK e ajustes de comandos ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.4] - OpenJDK from AdoptOpenJDK e ajustes de comandos ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.3] - Ajustes de comandos errados e adicionando Pop!_OS ([@thtmorais](https://gitlab.com/thtmorais))
- Comando errado: `upgrate - y`
- Instalação do `snapd`

### [0.0.2] - .gitignore ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.1] - Remoção `.idea` ([@thtmorais](https://gitlab.com/thtmorais))

### [0.0.0] - Instação e configuração no UBUNTU ([@thtmorais](https://gitlab.com/thtmorais))
- Ubuntu 20.04 LTS
